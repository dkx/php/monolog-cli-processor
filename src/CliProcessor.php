<?php

declare(strict_types=1);

namespace DKX\MonologCliProcessor;

use Monolog\Processor\ProcessorInterface;

final class CliProcessor implements ProcessorInterface
{
	/** @var bool */
	private $cliMode;

	/** @var array */
	private $argv;

	public function __construct(bool $cliMode = null, array $argv = null)
	{
		if ($cliMode === null) {
			$cliMode = \PHP_SAPI === 'cli';
		}

		if ($argv === null) {
			$argv = $_SERVER['argv'] ?? [];
		}

		$this->cliMode = $cliMode;
		$this->argv = $argv;
	}

	public function __invoke(array $record): array
	{
		if (!$this->cliMode) {
			return $record;
		}

		if (!\array_key_exists('extra', $record)) {
			$record['extra'] = [];
		}

		$record['extra']['cli'] = [
			'command' => $this->argv[0] ?? null,
			'args' => count($this->argv) > 0 ? \array_slice($this->argv, 1) : [],
		];

		return $record;
	}
}
