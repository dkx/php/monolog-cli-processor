# DKX/PHP/MonologCliProcessor

CLI processor for monolog

## Installation

```bash
$ composer require dkx/monolog-cli-processor
```

## Usage

```php
<?php

use DKX\MonologCliProcessor\CliProcessor;
use Monolog\Logger;

$logger = new Logger('default');
$logger->pushProcessor(new CliProcessor());
```
