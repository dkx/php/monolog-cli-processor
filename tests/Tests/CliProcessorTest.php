<?php

declare(strict_types=1);

namespace DKX\MonologPsrHttpRequestProcessorTests\Tests;

use DKX\MonologCliProcessor\CliProcessor;
use PHPUnit\Framework\TestCase;

final class CliProcessorTest extends TestCase
{
	public function testHandle_disabled(): void
	{
		$processor = new CliProcessor(false);
		$record = $processor([]);

		self::assertEquals([], $record);
	}

	public function testHandle_enabled(): void
	{
		$processor = new CliProcessor(true, ['bin/console', '--help']);
		$record = $processor([]);

		self::assertEquals([
			'extra' => [
				'cli' => [
					'command' => 'bin/console',
					'args' => ['--help'],
				],
			],
		], $record);
	}
}
